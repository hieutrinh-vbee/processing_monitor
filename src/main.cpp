#include <iostream>
#include <thread>
#include <map>
#include <vector>
#include "config.hpp"
#include "utility/utility.hpp"
#include <unistd.h>
#include "app_monitor.hpp"

using namespace CommonEngine;

int main(int argc, char const *argv[]) {
    std::cout << "START PMAPP VERSION 1.0.0" << std::endl;
    std::string appPath(argv[1]);
    // init and load configuration
    ConfigHandler *config_handler;
    std::string config_path = appPath + "config/pm_app.json";
    config_handler = new ConfigHandler(config_path);
    std::map<std::string, ConfigInfo> configs;
    config_handler->getConfig(configs);

    std::map<std::string, ConfigInfo>::iterator map;
    for(map = configs.begin(); map != configs.end(); map++){
        ConfigInfo app_info = map->second;
        AppMonitor *app_montior = new AppMonitor(app_info);
        std::thread AppMonitorTask(&AppMonitor::run, app_montior);
        AppMonitorTask.detach();
        usleep(300000);
    }

    while (1){
        sleep(100);
    }
    

    std::cout << "STOP PMAPP...." << std::endl;
}